# January 2020
#January #2020 #PC #snes #vita

I&#39;ve decided that in 2020 I&#39;ll start actively tracking my playthroughs and markdown my thoughts along the way. I mostly play causal games and R6 Siege but i look to expand the games i play in this new year. I hope to also improve my writing skills along the way. I&#39;m not the best at transforming my thoughts into written form but I will try my best.

I have a couple of gaming goals for this year:

1. Play more rpgs, I rarely ever play rpgs because of their length but I decided I want to give them a try
2. Play through all of the Tomb Raider games. Love the series but never completed the older games, I will replay the newer titles as well
3. Play older games I&#39;ve missed out on
4. Clear some backlog games
5. Play other games i wouldn&#39;t normally play like point & clicks etc.

## Vampire: The Masquerade - Bloodlines (*Completed*) - PC

![Vampire The Masquerade bloodlines](_v_images/20200518154448768_16495.jpg)
First up: Vampire: The Masquerade - Bloodlines. So, I don&#39;t really know much about this game, I&#39;ve watch a quick video on it and it looked cool so i bought on it GOG. It was the first game i bought for PC in years. I installed the unofficial patch and 2 more mods so I’m not playing it vanilla but I am doing a blind playthrough. There is a lot to learn about the different clans but I&#39;m slowly learning. First playthrough, only did the main quests.

**What I Like:**

- Multiple ways to complete missions
- great atmosphere

**What I dislike:**

- combat is a clunky

**Rating: 7/10**

## BioShock 2 (*Completed*) - PC

![Bioshock 2](_v_images/20200518154501091_3014.jpg)

So I bought the bioshock collection on steam a year ago but never got around to play it. I played Bioshock Infinite on PS3 years ago but now I&#39;m playing the series in order. I finished Bioshock 1 late last year and it was great.

**What I Like:**

- great atmosphere
- playing from a different perspective than the first game
- you don&#39;t have to switch guns and plasma
- Hacking was better

**What I dislike:**

**Rating:7/10**

## Rage - PC

![Rage](_v_images/20200518153653352_28124.jpg)

I got Rage for free on steam a while back and all i know is that its a FPS. I enjoy FPS games so i figured i give it a try.

**What I Like:**

- Driving is fun

**What I dislike:**

- Everything is kinda bland
- enimens take a lot of bullets to kill

## My Friend Pedro (*Completed*) - PC

![My Friend Pedro](_v_images/20200518154549587_30328.jpg)
After I completed Vampire: The Masquerade - Bloodlines, I deceived to play a quick &quot;filler&quot; game in between the other games.

**What I Like:**

- Hotline Like gameplay
- Fun gameplay

**What I dislike:**

- Strong first section of levels but falls off after that

**Rating: 6/10**

## Bastion (*Completed*) - PC

![Bastion](_v_images/20200518154608795_16128.jpg)

Another “filler” game. I’ve had this game for a while and i finally decided to give it a try after hearing how great it is. It was fun and short, kinda of a sad story. The way the story was told was well done.

**What I Like:**

- Great narration
- beautiful art style.

**What I dislike:**

- Gameplay is a little weak

**Rating: 7/10**

## Metal Slug X (*Completed*) - PC

![Metal Slug X](_v_images/20200518154622260_25462.jpg)

I&#39;ve Played Metal Slug games in the past but never completed them. I played this on free play and i used 33 counties. Not an easy game but really fun gameplay.

**What I Like:**

- Great graphics
- really fun

**What I dislike:**

- difficult

**Rating: 6/10**

## Two Point Hospital - PC

![Two Point Hospital](_v_images/20200518153711630_14889.jpg)

So I&#39;ve played theme hospital before after watching an LRG review on it. Rollercoaster tycoon is one of my fav games of all time so i knew theme hospital was right up my alley. This game is like crack.

**What I Like:**

- Cute Graphics
- Cute Animations

**What I dislike:**

- wish there was a faster speed

## Chrono Trigger - SNES

![Chrono Trigger](_v_images/20200518153725150_27769.png)
My second RPG of the new year. When I&#39;ve&#39;ve played RPGS iI&#39;veve played action RPGs so this is very different for me.

**What I Like:**

- Graphics hold up
- intersting story so far

**What I dislike:**

- some things aren&#39;t really obvious

## SteamWorld Dig - Vita

![Stream World Dig](_v_images/20200518153759047_23954.jpg)
My First vita game, its a mining game with cool looking robots.

**What I Like:**

- Beautiful Graphics

**What I dislike:**