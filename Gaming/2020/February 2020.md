# February 2020
#2020 #February #PC #Vita #SNES #PSP

# *Updates*

## Rage - PC

![Rage](_v_images/20200518153653352_28124.jpg)
**What I Like:**

- Driving is fun

**What I dislike:**

- Everything is kinda bland
- eminies take a lot of bullets to kill

## Two Point Hospital - PC

![Two Point Hospital](_v_images/20200518153711630_14889.jpg)

**What I Like:**

- Cute Graphics
- Cute Animations

**What I dislike:**

- wish there was a faster speed

## Chrono Trigger - SNES

![Chrono Trigger](_v_images/20200518153725150_27769.png)
**What I Like:**

- Graphics hold up
- intersting story so far

**What I dislike:**

- some things aren&#39;t really obvious

## SteamWorld Dig - Vita

![Stream World Dig](_v_images/20200518153759047_23954.jpg)

**What I Like:**

- Beautiful Graphics
- good for short bursts of gameplay

**What I dislike:**

# *New Games*

## Darksiders (*Completed*) - PC

![Darksiders](_v_images/20200518153827496_18717.jpg)

First up for February is a game called darksiders, i bought the bundle a while back but never played it. I honestly didn&#39;t know what the game was about when i bought, i just knew it was third person and zelda like. I never played a zelda game before so i can&#39;t really compare it.

I&#39;ve finally beat the spider boss.

**What I Like:**

- Cutscene are great
- opening scene was well done
- Combat is fun
- Good graphics

**What I dislike:**

- Flying section with the controller i was using
- spider boss sucks
- Game is too long
  **Rating: 6/10**

## Kentucky Route Zero (*Completed*) - PC

![kentucky-route-zero](_v_images/20200518153900980_1325.jpg)

I haven&#39;t played many point & click adventure games but I figure i might try some out. First up is Kentucky Route Zero. I know nothing about besides the fact that its a point & click adventure game.

Its How Long To Beats game of the month for [February](https://howlongtobeat.com/forum?&game=4956&thread=1882).

**What I Like:**

- beautiful graphics
- creepy atmosphere

**What I dislike:**

- Story is slow moving
- mute for the most part

**Rating: 6/10**

## Tom Clancy&#39;s Splinter Cell - PC

![Splinter Cell](_v_images/20200518153923098_25433.jpg)

This is a game I&#39;ve had for a while. this game hard af lmao

## Alan Wake (*Completed*) - PC
![Alan Wake](_v_images/20200518153933951_9835.jpg)

**What I Like:**

- atmosphere
- graphics

**Rating: 7/10**

## Rayman Origins (*Completed*) - Vita

![Rayman Origins](_v_images/20200518154008238_18015.jpg)

Started this game a year ago

**What I Like:**

- beautiful graphics
- Fun Gameplay
- Cute characters

**What I dislike:**

- Some levels were really difficult

**Rating: 7/10**

## Lego Batman: The Video Game - PSP

![lego batman](_v_images/20200518154025250_25914.jpg)

**What I Like:**

- Cute

**What I dislike:**

- Graphics aren&#39;t great on the psp version.

## Freddi Fish and the Case of the Missing Kelp Seeds (*Completed*) - PC

![Freddi Fish](_v_images/20200518154038500_7672.jpg)

**Rating: 6/10**

## Half-Life (*Completed*) - PC

![Half-Life](_v_images/20200518154048671_16255.jpg)

**Rating: 7/10**