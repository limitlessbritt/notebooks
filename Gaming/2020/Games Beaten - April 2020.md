# Games Beaten - April 2020
#pc #april #2020 #beaten

## Recap

The Witcher - PC
198x - PC

## The Witcher - PC

![The Witcher](_v_images/20200518154210944_27253.jpg)

**Rating: 8/10**

## 198x - PC

![198x](_v_images/20200518154220247_25340.png)

**Rating: 6/10**