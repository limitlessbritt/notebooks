# Games Beaten - March 2020
#pc #march #2020 #beaten

## Recap

I&#39;m late to posting but last month I beat 6 games. Sleeping Dogs was the best for me. It was fun, had a good story and a great world. So far its my game of the year but its still early so who knows. Binary Domain was a nice surprise. I knew nothing going into but it was really fun, the story was wild but at least it was wild from start to finish. Lastly, Tomb Raider 1 was fun but i got stuck a few times because it really doesn&#39;t not hold your hand. The controls were fine for me because the platforming was grid based. The last levels were hell for me though. Overall a good month!

Sleeping Dogs - PC
Stikbold! A Dogeball Adventure - PC
Binary Domain - PC
Tomb Raider - PC
Doom - PC
Glass Masquerade - PC

# **Sleeping Dogs - PC**

![Sleeping Dogs](_v_images/20200518154800293_32632.jpg)

I first started this game on PS3 but never finished. The game on PC looks great, the gameplay is fun. I had a really good time with this one, I would recommend it.

**Rating: 9/10**

# **Stikbold! A Dodgeball Adventure - PC**

![Stikbold](_v_images/20200518155029040_28244.jpg)

**Rating: 7/10**

# **Binary Domain - PC**

![Binary Domain](_v_images/20200518155040721_6759.jpg)

**Rating: 8/10**

# **Tomb Raider - PC**

![Tomb Raider](_v_images/20200518154812374_23100.jpg)

**Rating: 7/10**

# **DOOM 2016 - PC**

![Doom 2016](_v_images/20200518154846335_31002.jpg)

**Rating: 6/10**

# **Glass Masquerade - PC**

![Glass Masqerade](_v_images/20200518154931906_31300.jpg)

**Rating: 7/10**