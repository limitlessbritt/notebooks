# Games To Beat 2020

#2020
Below are games that I want to complete before the year ends. *Subject to change*

- [ ] Tomb Raider
- [ ] Tomb Raider 2
- [ ] Tomb Raider 3
- [ ] Rise of the Tomb Raider
- [ ] Tomb Raider 2013
- [ ] Shadow of the Tomb Raider
- [x] Darksiders
- [ ] Darksiders 2
- [x] Sleeping Dogs
- [ ] Tom Clancy&#39;s Splinter Cell
- [ ] Tokyo Xanadu eX+
- [ ] Threads of Fate
- [ ] Lara Croft Go
- [ ] Lara Croft and the Guardian of Light
- [ ] Grand Theft Auto: San Andreas
- [ ] At least 1 zelda game

Goal Count Total: 22
Backlog: 442
Last Years Total: 30
2018 Total: 20

Total Beat 2020: 11

# Outline of Goals for 2020

1. Play more rpgs, I rarely ever play rpgs because of their length but I decided I want to give them a try
2. Play through all of the Tomb Raider games. Love the series but never completed the older games, I will replay the newer titles as well
3. Play older games I&#39;ve missed out on
4. Clear some backlog games
5. Play other games i wouldn&#39;t normally play like point & clicks etc.