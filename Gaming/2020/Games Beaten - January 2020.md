# Games Beaten - January 2020
#2020 #january #beaten #PC

## Recap

I&#39;ve completed 5 games this month.

## **Vampire: The Masquerade - Bloodlines - PC**

![Vampire The Masquerade bloodlines](_v_images/20200518154448768_16495.jpg)

What a game, I can see why this game gets the high praise it does. Granted i only played the main quests but even that was great. Some parts of the game were really difficult like the sewer level.
**Rating: 7/10**

## **My Friend Pedro - PC**

![My Friend Pedro](_v_images/20200518154549587_30328.jpg)

This was a fun action game, it reminded me of Hotline Miami. Loved the first few levels but then i didn&#39;t really enjoy the last few levels. Story wasn&#39;t great and neither was the ending.
**Rating: 6/10**

## **Bastion - PC**

![Bastion](_v_images/20200518154608795_16128.jpg)
I&#39;ve heard a lot of great things about Bastion but I didn&#39;t really know much about it before playing it. After completing it I can say it is a great game. The gameplay was fun but not innovating but thats okay because what it lacks in gameplay it makes up for it with the interesting story,stunning graphics and the beautiful narration.
**Rating: 7/10**

## **Metal Slug X - PC**

![Metal Slug X](_v_images/20200518154622260_25462.jpg)

Metal Slug X is a fast paced run & gun game. Its pretty challenging but really fun at the same time. Animations were smooth and graphics were well done. Worth a play through, even better when played co-op. 
**Rating: 6/10**

## **BioShock 2 - PC**

![Bioshock 2](_v_images/20200518154501091_3014.jpg)
Bioshock 2 was a solid game. Gameplay wise, its way better than bioshock one were you dual wield gun and plasmids. I liked the hacking a lot more in this game as well. I liked how they did the tonic slots better in this game. Story wise playing as a big daddy was an interesting change from the first game. Overall worth a play even if people say its worse than the first.
**Rating: 7/10**