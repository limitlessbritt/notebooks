# Games Beaten - February 2020
#2020 #february #beaten #PC #Vita

## Recap

This month I&#39;ve beat 6 games. It was a mix bag of games including the HLTB game of the month. The best games was Rayman Origins and Half Life. Both were really enjoyable and a blast to play. The worst was KRZ and Freddi Fish. KRZ was just too slow moving for me and i wasn&#39;t really enjoying the story but it was a beautiful game.

Kentucky Route Zero - PC
Rayman Origins - Vita
Alan Wake - PC
Darksiders - PC
Freddi Fish and the Case of the Missing Kelp Seeds - PC
Half Life - PC

## **Kentucky Route Zero - PC**

![kentucky-route-zero](_v_images/20200518153900980_1325.jpg)

So this is my first point & click game in years but I figured I would give it a try. The graphics are beautiful and the atmosphere is perfectly creepy. However despite these positives i couldn&#39;t get past the slow moving story as well as the lack of an interesting story at that.

**Rating: 6/10**

## **Rayman Origins - Vita**

![Rayman Origins](_v_images/20200518154008238_18015.jpg)

This is one of the better platformers I&#39;ve played. Super fun, fast paced and challenging. Some levels had annoying parts but overall the level design was good. Playing this game on the vita was perfect because there was touch controls but it wasn&#39;t forced on you.

**Rating: 7/10**

## **Alan Wake - PC**

![Alan Wake](_v_images/20200518153933951_9835.jpg)

I&#39;ve heard a lot of great things about Alan Wake and it holds up to the praise. It has a great story, perfect atmosphere, good voice acting and sound design. The only downsides are the fact that it gets repetitive as you keep playing and there is no variety in the enemies you face. This is a great story based game that being said, I wouldn&#39;t play this game for the gameplay.

**Rating: 7/10**

## **Darksiders - PC**

![Darksiders](_v_images/20200518153827496_18717.jpg)

Personally i think the game over stayed its welcome. It started off really while but towards the end it was starting to drag. Combat was fun, the graphics still hold up, and the story was good. The gameplay was enjoyable but got repetitive. My main issues with the game is the length and the backtracking. I think this game is worth a play despite the downfalls.

**Rating: 6/10**

## **Freddi Fish and the Case of the Missing Kelp Seeds - PC**

![Freddi Fish](_v_images/20200518154038500_7672.jpg)

I played this game as a kid but didn&#39;t complete it so I decided to go back and play it. Its a kids game so the puzzles were very easy. The graphics were good and the story was okay. Overall not a bad kids game.

**Rating: 6/10**

## **Half Life - PC**

![Half-Life](_v_images/20200518154048671_16255.jpg)

First time playing this game that is referred to as a &quot;classic&quot;. Going into it i didn&#39;t know much besides the fact that people love it. It&#39;s safe to say that it holds up to the praise. The controls were great but a little floaty. Story was compelling, gameplay was fun and graphics were okay. Half-Life has a lot of really interesting gameplay mechanics and it switches up frequently and it keeps it fresh. What brings the game down for me was the first section of the game, it was too slow moving for me.

**Rating: 7/10**