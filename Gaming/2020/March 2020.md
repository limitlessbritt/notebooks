# March 2020
#march #pc #2020

# *Updates*

# *New Games*

## Sleeping Dogs (*Completed*) - PC

![Sleeping Dogs](_v_images/20200518154800293_32632.jpg)

I started this game on PS3 some years ago and it was one of the first few games i bought when i first built my pc in 2016. I&#39;ve never competed it but here I am, starting it over for the 3rd time.

**What I Like:**

- Fun gameplay
- Great port, easy to switch from controller to mouse and keyboard

**What I dislike:**

- That thousand knife side mission

**Rating: 9/10**

## Tomb Raider (*Completed*) - PC

![Tomb Raider](_v_images/20200518154812374_23100.jpg)

So first up on the tomb raider marathon is the very first tomb raider. I&#39;ll be playing the PC version.

## BioShock Infinite - PC

![BioShock Infinite](_v_images/20200518154830810_14315.jpg)

This is a replay, i first played the game on PS3.

## Doom 2016 (*Completed*) - PC

![Doom 2016](_v_images/20200518154846335_31002.jpg)
doom eternal is coming out soon so i figured i would play the reboot.

## Glass Masquerade - PC

![Glass Masqerade](_v_images/20200518154931906_31300.jpg)

I&#39;ve decided to play some chill after playing doom.