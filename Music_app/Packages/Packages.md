# Packages

## Navigation
easy navigation
```
https://pub.dev/packages/get
```

## XML 2 Json
convert xml response to json
```
https://pub.dev/packages/xml2json
```
## HTTP
```
https://pub.dev/packages/http
```
## XML
```
https://pub.dev/packages/xml
```