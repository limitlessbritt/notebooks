# Test Server

## Airsonic
Airsonic is going to be my test server for development 

```
cd C:\Users\ProcessHER\Dev\Airsonic
```
```
java -jar airsonic.war
```
admin
demo - demo1

Location: localhost:8080
location: http://192.168.0.11:8080
##  Docs 

[Airsonic Docs](https://airsonic.github.io/docs/configure/standalone/)

## Navidrome
```
C:\Users\ProcessHER\Dev\navidrome\navidrome.exe -configfile "C:\Users\ProcessHER\Dev\navidrome\navidrome.toml"
```

[navidrome](http://192.168.0.15:4533)