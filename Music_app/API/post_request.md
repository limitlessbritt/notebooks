# Post Request

The post request below uses subsoinc api 1.12.0
```
          onPressed: () async {
           String urL = s + rest + "u=" + u + "&p=" + password + "&v=" + v + "&c=" + c;
           //Example: "https://Mywebsite/api/values/" +_mailController.text +"/" + _passController.text;
           var reponse = await http.get(urL,
           headers: {"Accept": "application/json"});
          },
```

### THIS IS THE WORKING POST REQUEST

```  
          onPressed: () async {
           String urL = s + rest + "u=" + u + "&t=" + makeToken(password, salt) + "&s=" + salt  + "&v=" + v + "&c=" + c;
           //Example: "https://Mywebsite/api/values/" +_mailController.text +"/" + _passController.text;
           var reponse = await http.get(urL,
           headers: {"Accept": "application/json"});
          },
```

### Salt generation
This code makes a random salt for the api request
```
String salt = randomToken(6);

final _random = Random();

String randomToken(int length) => String.fromCharCodes(
      List.generate(length, (_) {
        var ch = _random.nextInt(52);
        if (ch > 25) {
          ch += 6;
        }
        return ch + 0x41;
      }),
    );

String newSalt() => randomToken(6);
```
### Token md5 generation
This code converts the password and combines the salt made beforehand for the api request.
```
String token = makeToken(password, salt);

String makeToken(String password, String salt) =>
    md5.convert(utf8.encode(password + salt)).toString().toLowerCase();
```

### Button Sign In

```
onPressed: () async {
           String urL = server + rest + "u=" + username + "&t=" + makeToken(password, salt) + "&s=" + salt  + "&v=" + version + "&c=" + client;
           //Example: "https://Mywebsite/api/values/" +_mailController.text +"/" + _passController.text;
           var reponse = await http.get(urL,
           headers: {"Accept": "application/json"});
            if (reponse.statusCode == 200){
                 return Navigator.push(context, MaterialPageRoute(builder: (context) => Home(),));
                } else{
               return AlertDialog(
                 title: Text('Error'),
                 content: SingleChildScrollView(
                   child: ListBody(
                     children: <Widget>[
                       Text('Wrong credentials please try again'),
                     ],
                     ),
                   ),
                   actions: <Widget>[
                     FlatButton(
                       child: Text('OK'),
                       onPressed: (){
                         Navigator.of(context).pop();
                       },
                       )
                   ],
               );
            }
          },
```

info back
id = int
userName = String
### Proper fucntion call
```
Future <List<Song>>fetchStarred() async{
final authresponse = await http.get(lovedsongsURL);
if (authresponse.statusCode == 200){
final jsondata = jsonDecode(authresponse.body);
var songsdata = apicallFromJson(jsondata);
var starred2 = songsdata.subsonicResponse.starred2.song;
return starred2;
}else
throw Exception("Unable to connect to server, try again");
}
```
