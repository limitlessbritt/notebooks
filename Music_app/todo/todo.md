# Todo

- [x] Validate user login against status?
- [ ] check if error code is present when user tries to log in
- [ ] Logged in State?
- [ ] figure out the url to send requests
- [ ] rework the login function to work properly
- [ ] create log out function
- [ ] get albums list, then use that list to grab album art to display?
- [ ] tell user if there was an error while trying to log in
- [x] fetch starred songs

### Login function

- [ ] set login state to keep user logged in when app is closed
- [ ] if user puts wrong credentials the first time it thinks wrong credentials all the time after that - the fix for this issuse problem is related to state management.
```
https://stackoverflow.com/questions/55604751/how-to-dynamically-setstate-variable-in-flutter
```
or this 
```
https://stackoverflow.com/questions/53820419/flutter-dart-calling-a-function-that-is-a-futurestring-but-needs-to-retu/53820672#53820672
```
this issue could be the value of status. as in the status value doesn't update.

### Homepage

- [ ] get bottom nav bar working

### Design

- [ ] design icon
- [ ] design logo
- [ ] design website mockup
- [ ] design app store page


### Album Covers Display
maybe create and index and a var, supply index with cover art ids and add that var into the fetchcoverart url for the function.


### DIO over HTTP?
Look into Dio package over http package, may be easy to work with?
```
https://pub.dev/packages/dio
```

### Audio Player Service
Look into the audio service package, could be useful.
```
https://pub.dev/packages/audio_service
```
Below is a tutorial:
```
https://github.com/ryanheise/audio_service/wiki/Tutorial
```

### Audio Player Package
Could us this package for playing server files
```
https://pub.dev/packages/just_audio
```

### Audio app Reference
```
https://github.com/rohansohonee/ufmp
```

### Cache Manager Package
Could be used to cache songs and images for offline playback and fasted load times?
```
https://pub.dev/packages/flutter_cache_manager
```

### If flutter ain't it
React Native or
```
https://cordova.apache.org/
```

### Pick A License
```
https://www.quora.com/Is-there-an-open-source-license-that-prevents-anyone-from-selling-my-software
```
or
```
https://opensource.org/licenses
```

### DataBase
```
https://pub.dev/packages/floor
```

### State Management?
```
https://pub.dev/packages/redux
```

### Cache Network Image
```
https://pub.dev/packages/cached_network_image
```

### HTTP package
```
https://pub.dev/packages/dio#handling-errors
```


### Streaming
```
https://pub.dev/packages/rxdart
```

### Wifi Streaming vs Data
```
https://pub.dev/packages/connectivity
```

### Permission handler
```
https://pub.dev/packages/permission_handler
```

### API IMAGE
```
https://stackoverflow.com/questions/53084913/how-to-display-images-received-from-an-api-response-in-flutter
```