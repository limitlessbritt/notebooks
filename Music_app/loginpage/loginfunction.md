# Login function

## Login, get user info XML to JSON
```
String urL = server + rest + "u=" + username + "&t=" + token  + "&s=" + salt + "&v=" + version + "&c=" + client;

class User {
  final int id;
  final int error;
  final String status;

  User({this.id, this.error, this.status});
  factory User.fromJson(Map<String, dynamic> json){
    return User(
      id: json['id'],
      error: json['error'],
      status: json['status'],
    );
  }
}

Xml2Json xml2json = new Xml2Json();

Future <User> fetchUser() async{
  final response = await http.get(urL,
  headers: {"Accept": "application/json"});
  xml2json.parse(response.body);
  var jsondata = xml2json.toGData();
  var data = json.decode(jsondata);
  return User.fromJson(data);

}
```

## Login button
```
        onPressed: () {
          fetchUser();
          if (status == "ok")
          return Get.toNamed('/home');
           else{
            return AlertDialog(
              title: Text("Error"),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text("Wrong credentials please try again"),
                  ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('OK'),
                    onPressed: (){
                      Navigator.of(context).pop();
                    },
                    )
                ],
            );
          } 
        },
```