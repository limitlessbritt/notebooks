# Basics

- import material package
- void main - runs app
- my app class is the main app
- edit pubspec.yaml to add local images files, added under flutter: assets: - “path to asset”\
- Void is a function
- add packages to pybspec.yaml then do &quot;flutter pub get&quot; in folder to get the packages

## Text

```
child: Text(
          &quot;Hello World&quot;,
          style: TextStyle(fontWeight: FontWeight.bold)
        ),
```

## Text Align

```
          child: Align(
            alignment: Alignment.topCenter,
            child: Text(&quot;Hello World&quot;)
```

## Aspect Ratio

```
children: &lt;Widget&gt;
          [
            new AspectRatio(aspectRatio: 4/3, child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))),
            new Text(&quot;Hello World&quot;)
          ],
```

## Baseline

```
child: Container(
            child: Row(
              children: &lt;Widget&gt;[
                new Text(&quot;Hello world&quot;,
                    style: new TextStyle(
                        color: Colors.pink,
                        fontSize: 28.0
                    )
                ),
                new Baseline(
                  baseline: -10,
                  baselineType: TextBaseline.alphabetic,
                  child: new Text(&quot;2&quot;),
                ),
                new Text(&quot;End&quot;)
              ],
            )
        ),
```

## Center

```
 child: Center(
            child: Text(&quot;Hello World&quot;)
          )
```

## Column

```
 child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
```

## Constrained Box

```
child: Container(
            child: Row(
              children: &lt;Widget&gt;[
                new Text(&quot;Hello world&quot;,
                    style: new TextStyle(
                        color: Colors.pink,
                        fontSize: 28.0
                    )
                ),
                new ConstrainedBox(
                  constraints: BoxConstraints(minWidth: 10),
                  child: new Text(&quot;WOW&quot;),
                ),
                new Text(&quot;End&quot;)
              ],
            )
        ),
```

## Container

```
child: Container(
          width: 250.0,
          height: 500.0,
          color: Colors.red,
          child: Text(
            &quot;Hello World&quot;,
            style: TextStyle(fontWeight: FontWeight.bold),
            textAlign: TextAlign.right,
          ),
        ),
```

## Image

```
child: Container(
          child: Image(
            //image: NetworkImage( &quot;https://media.comicbook.com/2019/02/the-batman-1159892-1280x0.jpeg&quot; ),
            image: AssetImage( &quot;assets/images/Batman.jpg&quot; ),
          )
        ),
```

## Padding

```
child: Container(
          child: Row(
            children: &lt;Widget&gt;[
              new Text(&quot;Hello world&quot;,
                  style: new TextStyle(
                    color: Colors.pink,
                    fontSize: 28.0,
                  )
              ),
              new Padding(
                //padding: EdgeInsets.all( 20.0 ),
                padding: EdgeInsets.fromLTRB(20.0, 0.0, 90.0, 0.0),
                child: new Text(&quot;WOW&quot;),
              ),
              new Text(&quot;End&quot;)
            ],
          )
        ),
```

## Row

```
child: Container(
          child: Row(
            children: &lt;Widget&gt;[
              new Text(&quot;Hello world&quot;,
                  style: new TextStyle(
                    color: Colors.pink,
                    fontSize: 28.0
                  )
              ),
              new Text(&quot;WOW&quot;),
            ],
          )
        ),
```

## Size Box

```
child: SizedBox(
          width: 300.0,
            child: Image(
              image: AssetImage( &quot;assets/images/Batman.jpg&quot; ),
            )
        ),
```

## Transform

```
child: Transform(
          origin: Offset(100, 100),
          transform: Matrix4.rotationZ(-0.785),
            child: Image(
              image: AssetImage( &quot;assets/images/Batman.jpg&quot; ),
            )
        ),
```