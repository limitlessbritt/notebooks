# Navigation

- Scaffold holds everything

## App Bar

```
void CallContact()
  {
    print(&quot;Calling contact&quot;);
  }

  void AddInfoToContact()
  {
    print(&quot;Adding Info To Contact&quot;);
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),

        actions: &lt;Widget&gt;[
          IconButton(
            icon: Icon(Icons.call),
            tooltip: &quot;Call Contact&quot;,
            onPressed: CallContact,
          ),
          IconButton(
            icon: Icon(Icons.add),
            tooltip: &quot;Add Information To Contact&quot;,
            onPressed: AddInfoToContact,
          )
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.

      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 50.0,

        )
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: &#39;Increment&#39;,
        child: Icon(Icons.airplay),
      ), // This t
      // railing comma makes auto-formatting nicer for build methods.
    );
  }
}
```

## Bottom Navigation Bar

```
return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),

        actions: &lt;Widget&gt;[
          IconButton(
            icon: Icon(Icons.call),
            tooltip: &quot;Call Contact&quot;,
            onPressed: CallContact,
          ),
          IconButton(
            icon: Icon(Icons.add),
            tooltip: &quot;Add Information To Contact&quot;,
            onPressed: AddInfoToContact,
          )
        ],
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.

      ),

      bottomNavigationBar: BottomNavigationBar(
        items: &lt;BottomNavigationBarItem&gt; [
          BottomNavigationBarItem(icon: Icon(Icons.home), title: Text(&quot;Home&quot;)),
          BottomNavigationBarItem(icon: Icon(Icons.business), title: Text(&quot;Business&quot;)),
          BottomNavigationBarItem(icon: Icon(Icons.school), title: Text(&quot;School&quot;)),
        ],
        currentIndex: _selectedIndex,
        onTap: ItemTapped,
        fixedColor: Colors.amber,
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: &#39;Increment&#39;,
        child: Icon(Icons.airplay),
      ), // This t
      // railing comma makes auto-formatting nicer for build methods.
    );
  }
```

## Drawer

```
        drawer: Drawer(
        child: Column(
          children: &lt;Widget&gt;[
            ListTile(
              leading: Icon(Icons.alarm),
              title: Text(&quot;Alarm&quot;),
              onTap: () {
                // Change the applications state
                print(&quot;Change page&quot;);
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
```

## Sliver App Bar

```
    return CustomScrollView(
      slivers: &lt;Widget&gt;[
        SliverAppBar(
          pinned: true,
          expandedHeight: 150,
          flexibleSpace: FlexibleSpaceBar(
            title: Text(&quot;Epic Sliver&quot;),
          ),
        ),
        SliverFixedExtentList(
          itemExtent: 50.0,
          delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.cyanAccent,
                  child: Text(&quot;List Item $index&quot;)
                );
              },
            childCount: 20
          ),
        )
      ],
    );
```

## Tab Bar View

```
    return MaterialApp(
      home: DefaultTabController(
          length: 3,
          child: Scaffold(
            appBar: AppBar(
              bottom: TabBar(
                tabs: [
                  Tab(icon: Icon(Icons.directions_railway)),
                  Tab(icon: Icon(Icons.directions_subway)),
                  Tab(icon: Icon(Icons.directions_bike)),
                ]
              ),

              title: Text(&quot;Tab Bar&quot;)
            ),

            body: TabBarView(
              children: [
                Icon(Icons.directions_railway),
                Icon(Icons.directions_subway),
                Icon(Icons.directions_bike),
              ]
            )
          )
      )
    );♣
```