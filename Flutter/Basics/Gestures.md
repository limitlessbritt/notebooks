# Gestures

- Use &quot;GestureDetector&quot; to call gestures

## Tap

```
child: Container(
          child: GestureDetector(
            onTap: ()
              {
                print(&quot;Tapped&quot;);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          )
        ),
```

## Double Tap

```
child: Container(
          child: GestureDetector(
            onDoubleTap: ()
              {
                print(&quot;Double tapped&quot;);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          )
        ),
```

## Long Press

```
 child: Container(
          child: GestureDetector(
            onLongPress: ()
              {
                print(&quot;Long Press&quot;);
              },
              onLongPressUp: ()
              {
                print(&quot;Long Press Released&quot;);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          )
        ),
```

## Horizontal Drag

```
        child: Container(
          child: GestureDetector(
            onHorizontalDragStart: (DragStartDetails details)
              {
                print(&quot;Start&quot;);
                print(details);
              },
              onHorizontalDragUpdate: (DragUpdateDetails details)
              {
                print(&quot;Update&quot;);
                print(details);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          ),
        ),
```

## Vertical Drag

```
        child: Container(
          child: GestureDetector(
            onVerticalDragStart: (DragStartDetails details)
              {
                print(&quot;Start&quot;);
                print(details);
              },
              onVerticalDragUpdate: (DragUpdateDetails details)
              {
                print(&quot;Update&quot;);
                print(details);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          )
        ),
```

## Pointers

```
        child: Container(
          child: Listener(
            onPointerDown: (PointerDownEvent event)
              {
                print(&quot;Clicked&quot;);
              },
              onPointerMove: (PointerMoveEvent event)
              {
                print(&quot;Moved&quot;);
                print(event);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          )
        ),
```

## Pan

```
        child: Container(
          child: GestureDetector(
              onPanStart: (DragStartDetails details)
              {
                print(&quot;Start&quot;);
                print(details);
              },

              onPanUpdate: (DragUpdateDetails details)
              {
                print(&quot;Update&quot;);
                print(details);
              },
              child: new Image(image: AssetImage(&quot;assets/images/Batman.jpg&quot;))
          )
        ),
```