# Limitlessbritt Server

### Server address
```
192.168.0.17
```


[server admin](https://192.168.0.17:8006/)

### Docker server
```
192.168.0.221
```

### VPN server
```
192.168.0.222
```
### DASH
```
http://192.168.0.221:32768
```

### FIX VPN
```
https://www.hungred.com/how-to/setup-openvpn-on-proxmox-lxc/
```

### To addd
* tincp
* navidrome
* bookstack
* calibre web
* kanboard
* statping
* dash machine

### Dash machine

```
[Media]
type = collection
icon = collections_bookmark
urls = {"url": "https://www2.himovies.to/", "icon": "static/images/apps/default.png", "name": "Movies 1", "open_in": "new_tab"},{"url": "https://ww5.fmovie.sc", "icon": "static/images/apps/default.png", "name": "Movies 2", "open_in": "new_tab"}
groups = public

[Design]
type = collection
icon = collections_bookmark
urls = {"url": "https://www.figma.com/", "icon": "static/images/apps/default.png", "name": "Figma", "open_in": "new_tab"},{"url": "https://www.behance.net", "icon": "static/images/apps/default.png", "name": "Behance", "open_in": "new_tab"},{"url": "https://coolors.co", "icon": "static/images/apps/default.png", "name": "Colors", "open_in": "new_tab"}
groups = public

[Dev]
type = collection
icon = collections_bookmark
urls = {"url": "https://devhints.io", "icon": "static/images/apps/default.png", "name": "Dev Hints", "open_in": "new_tab"},{"url": "https://devdocs.io", "icon": "static/images/apps/default.png", "name": "Dev Docs", "open_in": "new_tab"},{"url": "https://stackoverflow.com", "icon": "static/images/apps/default.png", "name": "Stackoverflow", "open_in": "new_tab"}
groups = public

[DevOps]
type = collection
icon = collections_bookmark
urls = {"url": "https://github.com", "icon": "static/images/apps/default.png", "name": "Github", "open_in": "new_tab"},{"url": "https://gitlab.com", "icon": "static/images/apps/default.png", "name": "GitLab", "open_in": "new_tab"},{"url": "http://127.0.0.1:8181", "icon": "static/images/apps/default.png", "name": "Jenkins", "open_in": "new_tab"}
groups = public

[Other]
type = collection
icon = collections_bookmark
urls = {"url": "https://howlongtobeat.com", "icon": "static/images/apps/default.png", "name": "HLTB", "open_in": "new_tab"},{"url": "https://www.goodreads.com", "icon": "static/images/apps/default.png", "name": "Goodreads", "open_in": "new_tab"},{"url": "https://trakt.tv/dashboard", "icon": "static/images/apps/default.png", "name": "Trakt", "open_in": "new_tab"}
groups = public

```