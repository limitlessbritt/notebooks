# Caddy and Airsonic

## Port-Forwarding

Login into to your router and port-forward 80 and 443

This video with help you:

https://www.youtube.com/watch?v=t4naLFSlBpQ

## Duckdns (Optional)

you can you any dns service you want. I used duckdns

1. go to duckdns.org and login using one of the services
  
2. create a domain name with duckdns
  
3. update your IP with duckdns
  
4. install duckdns using one of its methods
  

## Install Caddy

this depends on what you are running airsonic off of. For me i&#39;m using a raspberry pi 3. You can refer to [this](https://caddyserver.com/v1/tutorial/beginner) to help you install it on your server.

## Create Caddy Folder

Create a folder to store caddy data, again this is dependent on your system.

## CaddyFile

Create a file called &quot;Caddyfile&quot; this is the configuration file for caddy. It has no file extension .

## Configure Caddy

Copy this text and past it into the caddyfile you just created

```
airsonic.mydomain.com {
    proxy / 127.0.0.1:8080 {
        transparent
    }
}
```

Replace the &quot;airsonic.mydomain.com&quot; with the dns you created or domain name you have.

Save the file

go into the caddy data folder you made and run caddy from there.