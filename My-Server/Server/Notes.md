# Notes

# To Do List

- [x] Install Transcoder for Airsonic!
- [ ] update Airsonic
- [x] mount drive that contains media files
- [x] Fail2ban Implement
- [x] Create 2 Other Domains
- [x] Run Domains through [Caddy](https://caddy.community/t/multiple-domains/1761)
- [x] [Setup Dashboard](https://github.com/kutyla-philipp/simple-dash) Or [This](https://github.com/rmountjoy92/DashMachine)
- [x] Create an Google Drive Type Server or just a photo server or all in one
- [x] switch to duckdns

# Docker

- Play around with docker and learn about it could be an alternative to an All-In-One

currently docker does no work on the PI3 debain 10.

I&#39;ll have to test on a debain VS machine instead

# Duck DNS

- maybe change to duckdns so i can have sub domains
- setup duckdns on router
- run duckdns domain through caddy

duckdns is now working through caddy

# Google Drive Type Server

### Droppy

[GitHub - silverwind/droppy: Self-hosted file storage](https://github.com/silverwind/droppy)

[GitHub - mickael-kerjean/filestash: 🦄 A modern web client for SFTP, S3, FTP, WebDAV, Git, Minio, LDAP, CalDAV, CardDAV, Mysql, Backblaze, ...](https://github.com/mickael-kerjean/filestash)

[Own Cloud](https://owncloud.org/)

[pydio](https://pydio.com/) - no easy install for pi

[Gossa](https://github.com/pldubouilh/gossa) - no user accounts or secure access

[Seafile - Open Source File Sync and Share Software](https://www.seafile.com/en/home/)

[GitHub - eikek/sharry: Sharry is a self-hosted file sharing web application.](https://github.com/eikek/sharry)

### All in One

[Yunohost](https://yunohost.org/](https://yunohost.org/) - no go on PI 10 which is the newest PI version

[Sandstorm](https://sandstorm.io/) - no go on PI

i need to test yunohost on pie VM to see how to set it up, if successful its possible to implement it on my actual pi - Update it does not work on 10 version

## Gallery

instead of using a all in one or a google drive type server on the pi i could use a gallery server instead.

[PiGallery 2](https://bpatrik.github.io/pigallery2/)

[GitHub - trebonius0/Photato: Photato - The personal Pictures gallery](https://github.com/trebonius0/Photato)

I&#39;m still looking for a lightweight gallery i can run on my PI

##### PiGallery 2

Can&#39;t install on the PI becaue of node.js being out of date on the PI for buster

##### Photato

- install exiftool

then runt the java

Can&#39;t use Photato on the PI because it uses way to much ram.

# Music Server

## Koel

- try out [koel](https://koel.dev/)

## Gonic

https://github.com/sentriz/gonic

lightweight alternative but its headless

# Other

#### Pihole

#### Backup Solutions

[GitHub - gilbertchen/duplicacy: A new generation cloud backup tool](https://github.com/gilbertchen/duplicacy)

[BorgBackup – Deduplicating archiver with compression and authenticated encryption](https://www.borgbackup.org/)

[borgmatic](https://torsion.org/borgmatic/)

#### Code-server

Doesn&#39;t work on PI 32bit but maybe useful in the future

#### Fossil

[Fossil: Compiling and Installing Fossil](https://fossil-scm.org/skins/ardoise/doc/trunk/www/build.wiki)

can&#39;t use on PI

#### Git

[Gitea](https://gitea.io/en-us/)

#### Vault - Secret Management

[Vault Curriculum - HashiCorp Learn](https://learn.hashicorp.com/vault)

#### ProGet - Package management

[ProGet | Package your Applications and Components &#8211; Inedo](https://inedo.com/proget)

# Caddy config with users

| # Caddy config |
| -------------- |
|                |
|                |
|                |
|                |
|                |
|                |