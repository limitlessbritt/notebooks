# My Server

My Hub: [limitlessbritt.duckdns.org](https://limitlessbritt.duckdns.org)

#### IP

```
192.168.0.28
```

#### Launch Caddy

```
cd /etc/caddy
```

```
caddy
```

#### Droppy Start

```
sudo droppy start -c /srv/droppy/config -f /media/pi/serverfiles/Cloud
```

#### Navidrome

```
cd /home/pi/navidrome && ./navidrome -configfile "/home/pi/navidrome/navidrome.toml"
```

#### Launch calibre-web

```
cd /home/pi/calibre-web
```

```
python3 cps.py
```

#### PSItransfer

```
cd /home/pi/psitransfer
```

```
npm start
```

#### Cloud 9 IDE

```
cd /home/pi/c9sdk
```

```
nodejs ./server.js -p 8181 -l 0.0.0.0 -a limitlessbritt:bcyo12345 -w environment
```

#### WIKI

```
cd /home/pi/wiki
```

```
node server
```

#### Gitea

```
sudo GITEA_WORK_DIR=/var/lib/gitea/ /usr/local/bin/gitea web -c /etc/gitea/app.ini -p 3131
```

#### Linx-server

```
./linx-server -bind 192.168.0.28:7000
```

#### Reminiscence

```
cd /home/pi/reminiscence
```

```
source venv/bin/activate
```

```
cd venv/reminiscence
```

```
python3 manage.py runserver  192.168.0.28:9000
```

#### Gogs

```
cd gogs
```

```
./gogs web -port 3838
```

#### Filestash

```
cd /home/pi/filestash
```

```
./filestash
```

#### Syncthing

```
syncthing
```

#### Statping

```
statping
```

#### Whiteboard

```
cd whitebophir-master && PORT=5001 npm start
```