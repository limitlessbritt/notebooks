# To Test

### Drone CI
```
https://drone.io/
```

### Docker and Freenas
```
https://www.youtube.com/watch?v=1Y_p33_lMRk
```

### Proxmox tut
```
https://www.youtube.com/watch?v=azORbxrItOo
```

### Proxmox tut 2
```
https://www.youtube.com/watch?v=JvDMLNAxYbI&feature=youtu.be&t=392
```

### Photos
```
https://github.com/viktorstrate/photoview
```

### Database
```
https://hub.docker.com/_/mysql
```


### Proxy
```
https://containo.us/traefik/
```

### Dev
```
http://www.koding.com/
```

### Bookmarking

```
https://hub.docker.com/r/shaarli/shaarli/
```

### cloud9 ide

```
https://hub.docker.com/r/linuxserver/cloud9
```
### Notes
```
https://www.bookstackapp.com/docs/admin/installation/#docker
```
### Keycloak
```
https://www.keycloak.org/getting-started/getting-started-docker
```
### My SQL

```
https://www.youtube.com/watch?v=kOrGN36ViaU
```
### Photos GO
```
https://github.com/photoprism/photoprism
```
### Notes
```
https://github.com/zadam/trilium
```
### Tuleap
```
https://docs.tuleap.org/installation-guide/docker-image.html
```
### Beehive
```
https://github.com/muesli/beehive
```
### File Server
```
https://pydio.com
```
### Nextcloud?
```
https://nextcloud.com
```

### Drive Stuff
add drive
```
https://www.hostfav.com/blog/index.php/2017/02/01/add-a-new-physical-hard-drive-to-proxmox-ve-4x-5x/
```
/media

to mount drive
mount -t ntfs-3g /dev/sdb1 /mnt/ntfs/ 


edit etc/pve/lxc/100.conf

ide0:/dev/

mp0: /media,mp=/media/files

51820

Music
Books
Photos
Repositories
Playlists
Cloud
###### SSH KEY FOR AUTO TRANSFER
```
https://stackoverflow.com/questions/9607295/calculate-rsa-key-fingerprint
```

##### Drive Errors
```
https://unix.stackexchange.com/questions/39905/input-output-error-when-accessing-a-directory
```
##### Mount Drive
```
https://linuxconfig.org/how-to-mount-partition-with-ntfs-file-system-and-read-write-access
```

### OPENMEDIAVAULT

```
https://www.youtube.com/watch?v=hFghdtQig1Q
```
vm
```
https://www.youtube.com/watch?v=RaY72s7fOf0
```

### linux
```
https://blog.linuxserver.io/2017/06/24/the-perfect-media-server-2017/
```
### soruce
```
https://docs.sourcegraph.com/?_ga=2.192392547.858480263.1592238158-63217165.1592238158#quickstart-guide
```

### Firefly
```
https://docs.firefly-iii.org/installation/docker
```
### smart deploy
```
https://www.smartdeploy.com/linus/?utm_source=linus&utm_campaign=ltt2020q1&utm_medium=sp
```
### Markdown
```
https://hub.docker.com/r/linuxserver/raneto
```

### Backups
```
https://hub.docker.com/r/pschiffe/borg/
```
### Easy Backups
```
https://hub.docker.com/r/linuxserver/duplicati/
```

### shell
```
https://dev.to/mskian/install-z-shell-oh-my-zsh-on-ubuntu-1804-lts-4cm4
```