# Java Basics

## What is a data type?
Its a set of values and set of operations on those values.
![Java Cheat Sheet](_v_images/20200518152003410_4483.png) 

# Built-in data types
![Built-in data types](_v_images/20200518152759924_12054.png)

# Math Library
![Math Library](_v_images/20200518153155461_23956.png)
