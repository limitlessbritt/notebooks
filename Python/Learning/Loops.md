# Loops

## While Function
* When the execution reaches the end of a “while” statement's block, it jumps back to the start to re-check the condition.
* You can press ctrl-c to interrupt an infinite loop. This hotkey stops Python programs.
* A “break” statement causes the execution to immediately leave the loop, without re-check the condition.
* A “continue” statement causes the execution to immediate jump back to the start of the loop and re-check the condition.
```
money = 0
while money < 5:
    money = money + 1
    if money == 3:
        continue #skips 3
    print ("your money is " + str(money))
```
## For Loop
* A “for" loop will loop a specific number of times.
* The range() function can be called with one, two, or three arguments.
* The break and continue statements can be used in for loops just like they're used in while loops.
```
total = 0
for num in range (101):
    total = total + num
print (total)
```