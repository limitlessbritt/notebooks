# Basics

* An instruction that evaluates to a single value is an expression. An instruction that doesn't is a statement.
* Data types: int, float, string
* Strings hold text and begin and end with quotes: ‘Hello world!'
* Values can be stored in variables: spam = 42
* Variables can be used anywhere values can be used in expressions: spam + 1
```
print ("Hello there!")
print ("What is your name?") #asks for name
Name = input()
print ("It is good to meet you, " + Name)
print ("The length of your name is:")
print (len(Name))
print ("What is your age?") #asks for age
Age = input()
print ("You will be " + str(int(Age) + 1) + " in a year")
```

* The execution starts at the top and moves down.
* Comments begin with a # character and are ignored by Python; they are notes & reminders for the programmer.
* Functions are like mini-programs in your program.
* The print() function displays the value passed to it.
* The input() function lets users type in a value.
* The len() function takes a string value and returns an integer value of the string's length.
* The int(), str(), and float() functions can be used to convert values' data type.

#### Print on different lines
```
print ("Hell yeah" + \
" this is tes")
```
### Assign Variables
Assign multiple variables in a list example:
```
cat = ['fat', 'orange', 'loud'] 
size, color, disposition = cat #assign multiple variables in a list
print (size)
print (color)
print (disposition)
```
assign multiple variables example:
```
size, color, disposition = "skinny", "black", "quiet" #assign multiple variables
print (size)
```
Swap multiple variables example:
```
a = "aaaa"
b = "bbbb"
a, b = b, a #swap variables
print (a)
```
Quick maths:
```
love = 5
love += 1 # this will increase love by 1
print (love)
```