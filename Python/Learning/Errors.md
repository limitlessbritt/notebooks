# Errors

* A divide-by-zero error happens when Python divides a number by zero.
* Errors cause the program to crash. (This doesn't damage your computer at all. It's just that the computer doesn't know how to carry out this instruction, so it immediately stops the program by “crashing” rather than continue.)
* An error that happens inside a try block will cause code in the except block to execute. That code can handle the error or display a message to the user so that the program can keep going.

```
#This program will error out
def div42by(divideBy):
    return 42 / divideBy

print (div42by(2))
print (div42by(12))
print (div42by(0)) #can't divide by zero so it crashed
print (div42by(1))
```

```
#This program will run
def div42by(divideBy):
    try:
        return 42 / divideBy
    except ZeroDivisionError: #you don't have to say which error to catch
        print ("Error you tried to divid by Zero")

print (div42by(2))
print (div42by(12))
print (div42by(0)) #can't divide by zero so it crashed
print (div42by(1))
```