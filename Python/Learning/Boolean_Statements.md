# Boolean & Statements

## Boolean

* The Boolean data type has only two values: True and False (both beginning with capital letters).
* Comparison operators compare two values and evaluate to a Boolean value: ==, !=, <, >, ⇐, >=
* == is a comparison operator, while = is the assignment operator for variables.
* Boolean operators (and, or, not) also evaluate to Boolean values.

```
name = "Alice"
if name == "Alice":
    print ("Hi Alice")
print ("Done")
```
## Statements
* An if statement can be used to conditionally execute code, depending on whether the “if” statement's condition is True or False.
* An elif (that is, “else if”) statement can follow an if statement. Its block executes if its condition is True and all the previous conditions have been False.
* An else statement comes at the end. Its block is executed if all of the previous conditions have been False.
* The values 0 (integer), 0.0 (float), and ‘‘ (the empty string) are considered to be “falsey” values. When used in conditions they are considered False. You can always see for yourself which values are truthy or falsey by passing them to the bool() function.
```
password = "swordfish"
if password == "swordfish":
    print ("Access Granted!")
else:
    print ("Wrong Password")
```
elif example:
```
name = "Bob"
age = 3000
if name == "Alice":
    print ("Hi Alice")
elif age < 12:
    print ("you are a kid")
elif age > 2000:
    print ("damn you old")
elif age > 100:
    print ("you kinda old")
```