# Lists

## Lists
* A list is a value that contains multiple values: [42, 3.14, ‘hello']
* The values in a list are also called items.
* You can access items in a list with its integer index.
* The indexes start at 0, not 1.
* You can also use negative indexes: -1 refers to the last item, -2 refers to the second to last item, and so on.
* You can get multiple items from the list using a slice.
* The slice has two indexes. The new list's items start at the first index and go up to, but doesn't include, the second index.
* The len() function, concatenation, and replication work the same way on lists that they do with strings.
* You can convert a value into a list by passing it to the list() function.

```
#LISTS
spam = ['cat', 'hat', 'bat']
#indexing
print (spam [0])

#slice
print (spam [1:3])
```

## Lists & Loops

* A for loop technically iterates over the values in a list.
* The range() function returns a list-like value, which can be passed to the list() function if you need an actual list value.
* Variables can swap their values using multiple assignment: a, b = b, a
* Augmented assignment operators like += are used as shortcuts.

```
for i in range (4): #this range is a list
    print (i)
```
List Loop Example:
```
supplies = ['pens', 'staplers', 'drugs', 'binders']
for i in range (len(supplies)): #list of supplies
    print ("index " + str(i) + " in supplies is: " + supplies[i])
```

## Methods
* Methods are functions that are “called on” values.
* The index() list method returns the index of an item in the list.
* The append() list method adds a value to the end of a list.
* The insert() list method adds a value anywhere inside a list.
* The remove() list method removes an item, specified by the value, from a list.
* The sort() list method sorts the items in a list.
* The sort() method's reverse=True keyword argument can make the sort() method sort in reverse order.
* These list methods operate on the list “in place”, rather than returning a new list value.

Method indexing, inserting, and removing example:
```
spam = ['hello', 'hi', 'howdy', 'hey yall']
print (spam.index('hello')) #method indexing
spam.append ('love') # add to end
print (spam)
spam.insert(1, 'drugs') #insert into list
print (spam)
spam.remove('hello') #remove
print (spam)
```
Method sorting:
```
numbers = [1, 2, 3, 4, 8, 5, 7, 9]
numbers.sort() # sorting the lists
print (numbers)
numbers.sort(reverse=True) # reverse the sorting order
print (numbers)
#can't sort numbers and letters
```
True alphabetical order sorting:
```
abc = ['A', 'C', 'd', 'E', 'F', 'b']
abc.sort()
print (abc)
#sorting a list with strings sorts by capitals then lowercase
abc.sort(key=str.lower) #this sorts in true alphabetical order
print (abc)
```
String Slices:
```
catName = "Zophie a Cat"
print (catName)
newcatName = catName[0:7] + "the" + catName[8:12]
print (newcatName)
```
Completely copy over a list and not a reference example:
```
import copy
copy.deepcopy()
```