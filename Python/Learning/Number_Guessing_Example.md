# Number Guessing Example

```
#This is a Guess the number game
import random

print ("Hello what is your name?")
Name = input()

print ("Welcome, " + Name + ", I'm thinking of a number from 1 to 20")
sercetNumber = random.randint(1, 20)

print ("DEBUG: Secret number is " + str(sercetNumber)) #will print the sercet number foor debugging 

for guessesTaken in range (1, 7):
    print ("Take a guess")
    guess = int(input())
    
    if guess < sercetNumber:
        print ("your guess is too low")
    elif guess > sercetNumber:
        print ("your guess is too high")
    else:
        break # This condition is for the correct guess

if guess == sercetNumber:
    print ("Congrats " + Name + ", You took " + str(guessesTaken) + " guesses")
else:
    print ("Nope the number was " + str(sercetNumber))
```